let workerModule;
let workerInstance;

function emitProcessEvent(event, ...args) {
	process.send({
		eventType: "emit",
		emitEvent: event,
		args: args
	});
}

function onUncaught(e) {
	if (e.message)
		e=e.message;

	emitProcessEvent("error",e);
}

function onProcessMessage(message) {
	switch (message.eventType) {
		case "new":
			try {
				workerModule=require(message.fileName);
				workerInstance=new workerModule(...message.args);
				workerInstance.emit=emitProcessEvent;
				process.send({
					eventType: "newResult"
				});
			}

			catch (e) {
				process.send({
					eventType: "newError",
					errorMessage: e.message
				});
			}
			break;

		case "call":
			let func=workerInstance[message.functionName];
			Promise.resolve()
				.then(()=>{
					if (!func)
						throw new Error("Function '"+message.functionName+"' does not exist");
				})
				.then(()=>{
					return func.apply(workerInstance,message.args);
				})
				.then((res)=>{
					process.send({
						eventType: "callResult",
						callId: message.callId,
						result: res,
					});
				})
				.catch((err)=>{
					if (err.message)
						err=err.message;

					process.send({
						eventType: "callError",
						callId: message.callId,
						errorMessage: err
					});
				});
			break;
	}
}

process.on("message",onProcessMessage);
process.on("uncaughtException",onUncaught);
process.on("unhandledRejection",onUncaught);
