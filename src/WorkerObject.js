const child_process=require('child_process');
const EventEmitter=require("events");

class WorkerObject extends EventEmitter {
	constructor(fileName) {
		super();
		this.fileName=fileName;
		this.nextCallId=1;
		this.callPromises={};
	}

	onSubProcessClose(returnCode) {
		if (this.waitPromiseFuncs)
			this.waitPromiseFuncs.resolve(returnCode);

		else
			this.emit("close",returnCode);
	}

	onSubProcessMessage(message) {
		let promise;

		switch (message.eventType) {
			case "newResult":
				this.newPromise.resolve(this);
				break;

			case "newError":
				this.newPromise.reject(new Error(message.errorMessage));
				break;

			case "callResult":
				promise=this.callPromises[message.callId];
				delete this.callPromises[message.callId];
				promise.resolve(message.result);
				break;

			case "callError":
				promise=this.callPromises[message.callId];
				delete this.callPromises[message.callId];
				promise.reject(new Error(message.errorMessage));
				break;

			case "emit":
				this.emit(message.emitEvent,...message.args);
				break;
		}
	}

	new(...args) {
		return new Promise((resolve, reject)=> {
			this.newPromise={resolve,reject};
			this.subProcess=child_process.fork(__dirname+"/worker-object-loader.js",[]);
			this.subProcess.on("message",this.onSubProcessMessage.bind(this));
			this.subProcess.on("close",this.onSubProcessClose.bind(this));
			this.subProcess.send({
				eventType: "new",
				fileName: this.fileName,
				args: args
			});
		});
	}

	call(functionName, ...args) {
		let callId=this.nextCallId;
		this.nextCallId++;

		let callPromise=new Promise((resolve, reject)=>{
			this.callPromises[callId]={
				resolve: resolve,
				reject: reject
			};

			this.subProcess.send({
				eventType: "call",
				functionName: functionName,
				args: args,
				callId: callId
			});
		});

		return callPromise;
	}

	wait() {
		return new Promise((resolve,reject)=>{
			this.waitPromiseFuncs={resolve,reject};
		});
	}

	terminate() {
		return new Promise((resolve,reject)=>{
			this.wait().then(resolve);
			this.subProcess.kill("SIGKILL");
		});
	}
}

module.exports=WorkerObject;
