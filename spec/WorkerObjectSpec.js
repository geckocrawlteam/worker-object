const WorkerObject=require("..");

describe("WorkerObject",()=>{
	it("fails if created without filename",(done)=>{
		let o=new WorkerObject();
		o.new().catch((e)=>{
			expect(e.message).toEqual('The "id" argument must be of type string. Received type undefined');
			done();
		});
	});

	it("fails if created with file that doesn't exist",(done)=>{
		let o=new WorkerObject("doesntexist.js");
		o.new().catch((e)=>{
			expect(e.message).toEqual("Cannot find module 'doesntexist.js'");
			done();
		});
	});

	it("fails if the exported object is not a class",(done)=>{
		let o=new WorkerObject(__dirname+"/workers/BadExportWorker.js");
		o.new().catch((e)=>{
			expect(e.message).toEqual("workerModule is not a constructor");
			done();
		})
	});

	it("fails if the constructor fails",(done)=>{
		let o=new WorkerObject(__dirname+"/workers/ThrowInConstructorWorker.js");
		o.new().catch((e)=>{
			expect(e.message).toEqual("this is an error");
			done();
		})
	});

	it("can be consructed with arguments",(done)=>{
		let o=new WorkerObject(__dirname+"/workers/MethodWorker.js");
		o.new(1,2).then(()=>{
			o.call("getCtorArgs").then((res)=>{
				expect(res.arg1).toEqual(1);
				expect(res.arg2).toEqual(2);
				done();
			});
		});
	});

	it("fails if a function doesn't exist",(done)=>{
		let o=new WorkerObject(__dirname+"/workers/MethodWorker.js");
		o.new().then(()=>{
			o.call("doesntExist").catch((err)=>{
				expect(err.message).toEqual("Function 'doesntExist' does not exist");
				done();
			});
		});
	});

	it("can call a func sync",(done)=>{
		let o=new WorkerObject(__dirname+"/workers/MethodWorker.js");
		o.new().then(()=>{
			o.call("syncFunc",1,2).then((res)=>{
				expect(res).toEqual("sync func result: 3");
				done();
			});
		});
	});

	it("can call a func async",(done)=>{
		let o=new WorkerObject(__dirname+"/workers/MethodWorker.js");
		o.new().then(()=>{
			o.call("asyncFunc",5,6).then((res)=>{
				expect(res).toEqual("async func result: 11");
				done();
			});
		});
	});

	it("handles errors for sync funcs",(done)=>{
		let o=new WorkerObject(__dirname+"/workers/MethodWorker.js");
		o.new().then(()=>{
			o.call("syncFailingFunc").catch((err)=>{
				expect(err.message).toEqual("sync func error");
				done();
			});
		});
	});

	it("handles errors for async funcs",(done)=>{
		let o=new WorkerObject(__dirname+"/workers/MethodWorker.js");
		o.new().then(()=>{
			o.call("asyncFailingFunc").catch((err)=>{
				expect(err.message).toEqual("async func error");
				done();
			});
		});
	});

	it("receives emitted events",(done)=>{
		let o=new WorkerObject(__dirname+"/workers/EventWorker.js");
		o.on("hello",(a,b)=>{
			expect(a).toEqual(1);
			expect(b).toEqual(2);
			done();
		});
		o.new().then(()=>o.call("emitEvent"));
	});

	it("receives uncaught exceptions",(done)=>{
		let o=new WorkerObject(__dirname+"/workers/ExceptionWorker.js");
		o.on("error",(e)=>{
			expect(e).toEqual("hello");
			done();
		});
		o.new().then(()=>o.call("doException"));
	});

	it("receives unresolved promises",(done)=>{
		let o=new WorkerObject(__dirname+"/workers/ExceptionWorker.js");
		o.on("error",(e)=>{
			expect(e).toEqual("hello");
			done();
		});
		o.new().then(()=>o.call("doReject"));
	});

	it("detects close",(done)=>{
		let o=new WorkerObject(__dirname+"/workers/TerminatedWorker.js");
		o.on("close",(res)=>{
			expect(res).toEqual(123);
			done();
		});
		o.new()
			.then(()=>{
				o.call("exitProcess");
			});
	})

	it("can wait for termination",(done)=>{
		let o=new WorkerObject(__dirname+"/workers/TerminatedWorker.js");
		o.wait()
			.then(()=>{
				done();
			});
		o.new()
			.then(()=>{
				o.call("exitProcess");
			});
	});

	it("can be terminated",(done)=>{
		let o=new WorkerObject(__dirname+"/workers/TerminatedWorker.js");
		o.new()
			.then(()=>{
				o.terminate().then(()=>{
					done();
				});
			});
	});

	//it("maintains run state");
});