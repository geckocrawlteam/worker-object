class ExceptionWorker {
	doException() {
		setTimeout(()=>{
			throw new Error("hello");
		},100);
	}

	doReject() {
		setTimeout(()=>{
			Promise.resolve()
				.then(()=>{
					throw new Error("hello");
				});
		},100);
	}
}

module.exports=ExceptionWorker;