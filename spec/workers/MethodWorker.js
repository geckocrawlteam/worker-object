class MethodWorker {
	constructor(a,b) {
		this.arg1=a;
		this.arg2=b;
	}

	getCtorArgs() {
		return {
			arg1: this.arg1,
			arg2: this.arg2
		}
	}

	syncFunc(a,b) {
		return "sync func result: "+(a+b);
	}

	syncFailingFunc() {
		throw new Error("sync func error");
	}

	asyncFunc(a,b) {
		return new Promise((resolve,reject)=>{
			resolve("async func result: "+(a+b));
		});
	}

	asyncFailingFunc() {
		return new Promise((resolve,reject)=>{
			reject("async func error");
		});
	}
}

module.exports=MethodWorker;